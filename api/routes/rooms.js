import express from 'express'
import { createRoom, deleteRoom, getRoom, getRooms, UpdateRoom } from '../controllers/room_controller.js';
import { verifyAdmin } from '../utils/verifyToken.js'

const router = express.Router();

router.post("/", verifyAdmin, createRoom)

router.put("/:id", verifyAdmin, UpdateRoom)

router.delete("/:id", verifyAdmin, deleteRoom)

router.delete("/:id/:hotelId", verifyAdmin, deleteRoom)

router.get("/:id", getRoom)

router.get("/", getRooms)


export default router;