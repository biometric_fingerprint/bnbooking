import express from 'express'
import { deleteUser, getUser, getUsers, updateUser } from '../controllers/user_controller.js';
import { verifyAdmin, verifyToken, verifyUser } from '../utils/verifyToken.js';

const router = express.Router();

// router.get("/checkauthentication", verifyToken, (req, res, next) => {
//     res.send("Hello, you are logged in!")
// })

// router.get("/checkuser/:id", verifyUser, (req, res, next) => {
//     res.send("Hello, you are logged in and can delete account!")
// })

// router.get("/checkadmin/:id", verifyAdmin, (req, res, next) => {
//     res.send("Hello, you are logged in and can delete all accounts!")
// })

router.get("/", verifyAdmin, getUsers)
router.put("/:id", verifyUser, updateUser)
router.delete("/:id", verifyUser, deleteUser)
router.get("/:id", verifyUser, getUser)

export default router;